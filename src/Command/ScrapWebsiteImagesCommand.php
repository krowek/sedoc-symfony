<?php

namespace App\Command;

use App\Service\ImageComparator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;


class ScrapWebsiteImagesCommand extends Command
{
    /** @var ImageComparator */
    private $imageComparator;

    /** @var OutputInterface */
    private $output;

    public function __construct(ImageComparator $imageComparator)
    {
        $this->imageComparator = $imageComparator;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('website:scrap:images')
            ->setDescription('Scrap and compare images from website.')
            ->setHelp('This command allows you to download images from provided website and check if some was changed')

            ->addArgument('url', InputArgument::REQUIRED, 'Website url')
            ->addArgument('destination', InputArgument::REQUIRED, 'Path to save images')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Image scraper',
            '============',
            '',
        ]);

        $url = rtrim($input->getArgument('url'), '/');
        $destination = rtrim($input->getArgument('destination'), '/');
        $this->output = $output;

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException('Invalid url');
        }
        if (!file_exists($destination)) {
            mkdir($destination);
        }

        $this->scrapImages($url, $destination);

        $output->writeln('Done.');
    }

    private function scrapImages(string $websiteUrl, string $destination)
    {
        foreach ($this->getImagesUrls($websiteUrl) as $imageUrl) {

            $imageUrl = $websiteUrl . $imageUrl;

            $imageName = pathinfo($imageUrl, PATHINFO_BASENAME);
            $path = $destination . '/' . $imageName;
            if (file_exists($path)) {

                $isIdentical  = $this->imageComparator->isIdentical($path, $imageUrl);
                if (!$isIdentical) {
                    $this->output->writeln('Image does changed: ' . $imageUrl);
                    $this->saveImage($imageUrl, $path);
                } else {
                    $this->output->writeln('Image does not changed: ' . $imageUrl);
                }
            } else {
                $this->output->writeln('Image scraping for the first time: ' . $imageUrl);
                $this->saveImage($imageUrl, $path);
            }
        }
    }

    private function saveImage($imageUrl, $path)
    {
        $image = file_get_contents($imageUrl);
        $result = file_put_contents($path, $image);

        if (!$result) {
            throw new \InvalidArgumentException('Could not save image to ' . $path);
        }
    }

    private function getImagesUrls(string $websiteUrl): array
    {
        $html = file_get_contents($websiteUrl);

        $crawler = new Crawler($html);

        $urls = $crawler
            ->filterXpath('//img')
            ->extract(array('src'));

        return $urls;
    }
}