<?php
namespace App\Service;

class ImageComparator
{
    public function isIdentical(string $file, string $url): bool
    {
        if (!is_file($file)) {
            throw new \InvalidArgumentException('Invalid file path ' . $file);
        }
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException('Invalid file path ' . $url);
        }

        $image1Hash = md5(file_get_contents($file));
        $image2Hash = md5(file_get_contents($url));

        return $image1Hash === $image2Hash;
    }


}